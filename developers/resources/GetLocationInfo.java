	// Override this method in your tool behavior provider
	@Override
	public ILocationInfo getLocationInfo(PictogramElement pe, ILocationInfo locationInfo) {
		PictogramElement[] selectedPictogramElements = getDiagramTypeProvider().getDiagramEditor().getSelectedPictogramElements();
		if (selectedPictogramElements != null && selectedPictogramElements.length > 0) {
			// Use the first selected pictogram element for direct editing
			PictogramElement pictogramElement = selectedPictogramElements[0];
			if (pictogramElement instanceof ContainerShape) {
				Object bo = getFeatureProvider().getBusinessObjectForPictogramElement(pictogramElement);
				if (bo instanceof EClass) {
					ContainerShape shape = (ContainerShape) pictogramElement;
					// Found the root container shape of a class
					Shape classNameShape = (shape).getChildren().get(1);
					GraphicsAlgorithm classNameGA = classNameShape.getGraphicsAlgorithm();
					if (classNameGA instanceof Text) {
						return new LocationInfo(classNameShape, classNameGA);
					}

				}
			}
		}
		return super.getLocationInfo(pe, locationInfo);
	}
