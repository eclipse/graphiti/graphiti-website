nameText.addModifyListener(new ModifyListener() {

	@Override
	public void modifyText(ModifyEvent e) {
		String value = nameText.getText();
		if (value == null) {
			value = "";//$NON-NLS-1$
		}
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			// the filter assured, that it is a EClass
			if (bo == null)
				return;
			String name = ((EClass) bo).getName();
			if (value.equals(name))
				return;
		}
		final String typedValue = value;
		IFeature feature = new AbstractFeature(getDiagramTypeProvider().getFeatureProvider()) {
				
			@Override
			public void execute(IContext context) {
				PictogramElement pe = getSelectedPictogramElement();
				if (pe != null) {
					Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
					// the filter assured, that it is a EClass
					if (bo == null)
						return;
					EClass eClass = (EClass) bo;
					eClass.setName(typedValue);
				}
			}
			
			@Override
			public boolean canExecute(IContext context) {
				return true;
			}
		};
		CustomContext context = new CustomContext();
		execute(feature, context);
	}
});